window._ = require('lodash');
window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
let token = document.head.querySelector('meta[name="_csrf_token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found');
}

import Vuetify from 'vuetify'
import Vue from 'vue'


/**
 * Initialization
 *
 */
Vue.use(Vuetify, {
	theme: {
		accent: '#74A56B',
		//primary: '#FFF'
		primary: '#3b3b3b',
		secondary: '#74A56B',
	}
});
Vue.component('dashboard', require('./templates/Dashboard.vue').default);
Vue.component('auth', require('./templates/Auth.vue').default);
Vue.component('admin', require('./templates/Admin.vue').default);


/**
 * Start
 *
 */
const site = new Vue({
    el: '#app',
});