# Kalories
#### Deploy instructions:
- Install symfony typing "php composer install" into a terminal opened in the root folder
- Make sure to create a database. You can setup database name and connection in .env
- Run the migrations typing "php bin/console doctrine:migrations:migrate"
- Set the app into "production" mode in .env
- If running locally, type "php bin/console server:run" in a terminal to launch the server. You can edit the url in .env, default is http://127.0.0.1:8000

#### URLs
- Login: http://127.0.0.1:8000/login
- Dashboard: http://127.0.0.1:8000/login
- Admin area: http://127.0.0.1:8000/admin

#### Generating an admin
Admin registration and user manipulation are not implemented, if you need an admin account you have to manually set the "is_admin" field of the corresponding user to "true" ("1" if your database uses "tinyint" instead of boolean fields).
If you are logged in with the newly promoted admin account when you perform this change, be sure to logout and login again.

#### Frontend
The frontend has been made with Vue.js, if you need to edit it you'll need to install the required node modules. For more informations read the Symfony docs about Encore: http://symfony.com/doc/current/frontend.html
