<?php

namespace App\Entity;

class Meal implements \Serializable
{
    protected $id;
    protected $datetime;
    protected $text;
    protected $calories;

    protected $user;

    /**
     * Getters
     *
     */
    public function getId()
    {
        return $this->id;
    }

    public function getDatetime()
    {
        return $this->datetime;
    }

    public function getCalories()
    {
        return $this->calories;
    }

    public function getUser()
    {
    	return $this->user;
    }


    /**
     * Setters
     *
     */
    public function setDatetime($datetime)
    {
        $this->datetime = $datetime;
        return $this;
    }

    public function setText($text)
    {
        $this->text = $text;
        return $this;
    }

    public function setCalories($calories)
    {
        $this->calories = $calories;
        return $this;
    }

    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }


     /**
     * UserInterface related method
     */
    public function eraseCredentials()
    {
        //
    }


    /**
     * Misc
     *
     */
    public function removeAttributes(array $attributes)
    {
    	foreach ($attributes as $attribute)
    		unset($this->$attribute);
    	return $this;
    }


    /**
     * Serializable methods
     */
    public function serialize()
    {
        return serialize([ $this->id, $this->date, $this->time, $this->text, $this->calories ]);
    }

    public function unserialize($serialized)
    {
        list($this->id, $this->date, $this->time, $this->text, $this->calories) = unserialize($serialized);
    }

}
