<?php

namespace App\Entity;

use Symfony\Component\Security\Core\User\UserInterface;

class User implements UserInterface, \Serializable
{
    protected $id;
    protected $username;
    protected $password;
    protected $is_admin = false;

    protected $meals;

    /**
     *	Getters
     */
    public function getId()
    {
        return $this->id;
    }

    public function getRoles()
    {
        if ($this->is_admin)
            return ['ROLE_USER', 'ROLE_ADMIN'];
        else
            return ['ROLE_USER'];
    }

    public function getPassword()
    {
    	return $this->password;
    }

    public function getSalt()
    {
    	return '-G-rW-sDd*w#X*VnV1mQOkFC&lG6&SXvf3yggNHr';
    }

    public function getUsername()
    {
    	return $this->username;
    }

    public function getMeals()
    {
        return $this->meals;
    }


    /**
     * Issers
     *
     */
    public function isAdmin()
    {
        return $this->is_admin;
    }



    /**
     *  Setters
     */
    public function setRoles()
    {
        if ($this->is_admin)
            return ['ROLE_USER', 'ROLE_ADMIN'];
        else
            return ['ROLE_USER'];
    }

    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    public function setIsAdmin($is_admin)
    {
        $this->is_admin = $is_admin;
        return $this;
    }

    public function setMeals($meals)
    {
        $this->meals = $meals;
        return $this;
    }


    /**
     * UserInterface related method
     */
    public function eraseCredentials()
    {
        //
    }


    /**
     * Misc
     *
     */
    public function removeAttributes(array $attributes)
    {
        foreach ($attributes as $attribute)
            unset($this->$attribute);
        return $this;
    }


    /**
     * Serializable methods
     */
    public function serialize()
    {
        return serialize([ $this->id, $this->username, $this->password, $this->meals ]);
    }

    public function unserialize($serialized)
    {
        list($this->id, $this->username, $this->password, $this->meals) = unserialize($serialized);
    }
}
