<?php

namespace App\Controller;

use App\Classes\Auth;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class UsersController extends Controller
{
    /**
     * Get current user
     *
     */
    public function user(TokenStorageInterface $token_storage)
    {
    	$user = $token_storage->getToken()->getUser();
    	($user && $user !== 'anon.') ? $response = [ 'username' => $user->getUsername() ] : $response = null;
        return $this->json($response);
    }
}
