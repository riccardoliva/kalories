<?php

namespace App\Controller;

use App\Entity\Meal;
use App\Classes\ResponseUtils;
use App\Classes\ValidationErrors;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class MealsController extends Controller
{
    /**
     * The current user ID
     *
     */
    private $user;

    /**
     * Istance of ResponseUtils
     *
     */
    private $utils;

    /**
     * The Request data
     *
     */
    private $data;

    /**
     * The meal repository
     *
     */
    private $meal_repository;

    /**
     * The meal we are currently manipulating
     *
     */
    private $meal;




    public function __construct(TokenStorageInterface $token_storage)
    {
        $this->user = $token_storage->getToken()->getUser();
        $this->utils = new ResponseUtils();
    }
    



    /**
     * Public endpoints
     *
     */


    /**
     * Return all the meals of the current user
     *
     */
	public function index()
	{
        $this->getMealRepository();
    	$this->meal = $this->meal_repository->findByUserId($this->user->getId());
    	$response = $this->utils->prepareJson($this->meal);
	    return new Response($response);
	}


    /**
     * Return a specific meal if it is associated to the current user, null otherwise
     *
     */
    public function show($id)
    {
        $this->getMealRepository();
        $this->meal = $this->meal_repository->findOneByUserId($id, $this->user->getId());
        $response = $this->utils->prepareJson($this->meal);
        return new Response($response);
    }


    /**
     * Insert a new Meal in database
     * @param Request string date, string time, string text, float calories
     *
     */
    public function store(Request $request, ValidatorInterface $validator)
    {
        $this->data = $this->utils->getData($request);
        $this->meal = new Meal();
        $this->buildMeal();

        $errors = new ValidationErrors($validator->validate($this->meal));
        if ($errors->hasErrors())
            return $this->json($errors->getErrorMessages());
        else
            $this->writeInDatabase('persist');

        return $this->json(['success' => true]);
    }


    /**
     * Edit a Meal in database
     * @param Request string date, string time, string text, float calories
     *
     */
    public function update(Request $request, ValidatorInterface $validator, $id)
    {
        $this->data = $this->utils->getData($request);
        $this->getMealRepository();
        $this->meal = $this->meal_repository->find($id);

        if (!$this->meal)
            return $this->json(['errors' => ["This meal doesn't exist"]]);

        if (!$this->userOwnsThisMeal())
            return $this->json(['errors' => ["This isn't your meal!"]]);

        $this->buildMeal(); 
        $errors = new ValidationErrors($validator->validate($this->meal));
        if ($errors->hasErrors())
            return $this->json($errors->getErrorMessages());
        else
            $this->writeInDatabase('persist');

        return $this->json(['success' => true]);
    }


    /**
     * Delete a meal
     *
     */
    public function destroy($id)
    {
        $this->getMealRepository();
        $this->meal = $this->meal_repository->find($id);

        if (!$this->userOwnsThisMeal())
            return $this->json(['errors' => ["This isn't your meal!"]]);
        else
            $this->writeInDatabase('remove');

        return $this->json(['success' => true]);
    }


    /**
     * Filter a user meal by date/time
     * @param Request string date, string time
     *
     */
    public function filter(Request $request)
    {
        $this->data = $this->utils->getData($request);
        $this->getMealRepository();
        $this->meal = $this->meal_repository->findByUserIdFiltered($this->user->getId(), $this->data);
        $response = $this->utils->prepareJson($this->meal);
        return new Response($response);
    }



    /**
     * For internal use
     *
     */

    /**
     * Assign properties
     *
     */
    private function buildMeal()
    {
        $this->meal->setDatetime(new \DateTime($this->data['date'] . ' ' . $this->data['time'] . ':00'));
        $this->meal->setText($this->data['text']);
        $this->meal->setCalories($this->data['calories']);
        $this->meal->setUser($this->user);
    }


    /**
     * Save/Update Meal
     *
     */
    private function writeInDatabase($operation)
    {
        $manager = $this->getDoctrine()->getManager();
        $manager->$operation($this->meal);
        $manager->flush();
    }


    /**
     * Init meal repo
     *
     */
    private function getMealRepository()
    {
        $this->meal_repository = $this->getDoctrine()->getManager()->getRepository(Meal::class);
    }


    /**
     * Check if a user owns a meal
     *
     */
    private function userOwnsThisMeal()
    {
        $meal_user_id = $this->meal->getUser()->getId();
        $user_id = $this->user->getId();
        return ($meal_user_id === $user_id);
    }


}
