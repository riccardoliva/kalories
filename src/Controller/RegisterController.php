<?php

namespace App\Controller;

use App\Entity\User;
use App\Classes\ResponseUtils;
use App\Classes\ValidationErrors;
use App\PasswordEncoder\Sha2PasswordEncoder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RegisterController extends Controller
{
    /**
     * Register a user
     *
     */
    public function register(Request $request, ValidatorInterface $validator, Sha2PasswordEncoder $encoder)
    {
        $utils = new ResponseUtils();
        $data = $utils->getData($request);
        $username = $data['username'];
        $password = $data['password'];

        $user = new User();
        $user->setUsername($username);
        $user->setPassword($password);
        $errors = new ValidationErrors($validator->validate($user));

        if ($errors->hasErrors())
            return $this->json($errors->getErrorMessages());

        $encrypted = $encoder->encodePassword($user->getPassword(), $user->getSalt());
        $user->setPassword($encrypted);

        $manager = $this->getDoctrine()->getManager();
        $manager->persist($user);
        $manager->flush();
        return $this->json(['success' => true]);
    }
}
