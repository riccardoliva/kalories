<?php

namespace App\Controller;

use App\Classes\ResponseUtils;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class OptionsController extends Controller
{

	/**
	 * The options file
	 *
	 */
	private $file = __DIR__.'/../../config/options.yaml';


	/**
	 * Get all the options
	 *
	 */
    public function index()
    {
        $options = Yaml::parseFile($this->file);
        return $this->json($options);
    }


    /**
     * Update the options file
     *
     */
    public function update(Request $request)
    {
    	$utils = new ResponseUtils();
    	$data = $utils->getData($request);
    	$content = Yaml::dump($data);
    	file_put_contents($this->file, $content);
    	return $this->json([ 'success' => true ]); 
    }
}
