<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class PagesController extends Controller
{
    public function index(TokenStorageInterface $token_storage)
    {
        $user = $token_storage->getToken()->getUser();
        if ($user)
            return $this->redirect('/dashboard');
        else
            return $this->redirect('/login');
    }

    public function login()
    {
        return $this->render('index.html.twig', [ 'tag' => '<auth></auth>' ]);
    }

    public function dashboard()
    {
        return $this->render('index.html.twig', [ 'tag' => '<dashboard></dashboard>' ]);
    }

    public function admin()
    {
        return $this->render('index.html.twig', [ 'tag' => '<admin></admin>' ]);
    }
}
