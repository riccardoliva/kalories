<?php

namespace App\Controller;

use App\Entity\User;
use App\Classes\ResponseUtils;
use App\PasswordEncoder\Sha2PasswordEncoder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class LoginController extends Controller
{

    /**
     * Login
     *
     */
    public function login(Request $request, Sha2PasswordEncoder $encoder, TokenStorageInterface $token_storage, EventDispatcherInterface $event_dispatcher, SessionInterface $session)
    {
        $utils = new ResponseUtils();
        $data = $utils->getData($request);
        $username = $data['username'];
        $password = $data['password'];

        $user = $this
                    ->getDoctrine()
                    ->getManager()
                    ->getRepository(User::class)
                    ->findOneBy([ 'username' => $username ]);

        // User not found
        if(!$user)
            return $this->json([ 'errors' => ['An error occured. Try checking your credentials.'] ]);
        // Wrong password
        if(!$encoder->isPasswordValid($user->getPassword(), $password, $user->getSalt()))
            return $this->json([ 'errors' => ['An error occured. Try checking your credentials.'] ]);

        // Set token
        $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
        $token_storage->setToken($token);
        $session->set('_security_main', serialize($token));

        // Login
        $event = new InteractiveLoginEvent($request, $token);
        $event_dispatcher->dispatch("security.interactive_login", $event);
        return $this->forward('App\Controller\UsersController::user', [ 'token_storage' => $token_storage ]);
        
    }


    /**
     * Logout
     *
     */
    public function logout(TokenStorageInterface $token_storage, SessionInterface $session)
    {
    	$token_storage->setToken(null);
        $session->invalidate();
        return $this->json(['success' => true]);
    }
}
