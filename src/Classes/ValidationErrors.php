<?php

namespace App\Classes;

use Symfony\Component\Validator\ConstraintViolationList;

class ValidationErrors
{
	/**
	 * Errors list
	 *
	 */
	protected $errors;


	public function __construct(ConstraintViolationList $errors)
	{
		$this->errors = $errors;
	}

	/**
	 * Returns true if there are validation errors
	 *
	 */
	public function hasErrors()
	{
		return (count($this->errors) > 0);
	}

	/**
     * Handle validation errors
     * @return array
     *
     */
    public function getErrorMessages()
    {
    	$list = [];
    	foreach ($this->errors as $error)
    	{
    		$message = $error->getMessage();
    		array_push($list, $message);
    	}
    	return [ 'errors' => $list ];
    }
}