<?php

namespace App\Classes;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class ResponseUtils
{
	/**
     * Return an array with the data of a request.
     * Useful if the request is an ajax.
     * @return array
     *
     */
    public function getData(Request $request)
    {
        return json_decode($request->getContent(), true);
    }


    /**
     * Handle serialization
     * @param array|object $data, array $exclusions (fields to unset)
     *
     */
    public function prepareJson($data, array $exclusions = [])
    {
        $encoder = new JsonEncoder();
        $normalizer = new ObjectNormalizer();
        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });

        $serializer = new Serializer(array($normalizer), array($encoder));
        return $serializer->serialize($data, 'json');
    }
}