<?php

namespace App;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class BaseController extends Controller
{
	


    /**
     * Return an array with the data of a request.
     * Useful if the request is an ajax.
     * @return array
     *
     */
    protected function getData(Request $request)
    {
        return json_decode($request->getContent(), true);
    }


    /**
     * Handle serialization
     * @param array|object $data, array $exclusions (fields to unset)
     *
     */
    protected function prepareJson($data, array $exclusions = [])
    {
        $encoder = new JsonEncoder();
        $normalizer = new ObjectNormalizer();
        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });

        $serializer = new Serializer(array($normalizer), array($encoder));
        return $serializer->serialize($data, 'json');
    }
}