<?php

namespace App\Repository;

use App\Entity\Meal;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * @method Meal|null find($id, $lockMode = null, $lockVersion = null)
 * @method Meal|null findOneBy(array $criteria, array $orderBy = null)
 * @method Meal[]    findAll()
 * @method Meal[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MealRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Meal::class);
    }

    /**
     * Get meals of a specific user
     *
     */
    public function findByUserId($user_id)
    {
    	return $this->createQueryBuilder('m')
            ->where('m.user = :user_id')
            ->setParameter('user_id', $user_id)
            ->select('m.id', 'm.datetime', 'm.calories', 'm.text')
            ->getQuery()
            ->getResult();
    }


    /**
     * Get a meal if it is of a user, otherwise return null
     *
     */
    public function findOneByUserId($id, $user_id)
    {
    	return $this->createQueryBuilder('m')
            ->where('m.user = :user_id')
            ->andWhere('m.id = :id')
            ->setParameter('user_id', $user_id)
            ->setParameter('id', $id)
            ->select('m.id', 'm.datetime', 'm.calories', 'm.text')
            ->getQuery()
            ->getOneOrNullResult();
    }


    /**
     * Get meals of a specific user, with datetime filters
     *
     */
    public function findByUserIdFiltered($user_id, array $filters)
    {
        return $this->createQueryBuilder('m')
            ->where('m.user = :user_id')
            ->andWhere('m.datetime >= :from')
            ->andWhere('m.datetime <= :to')
            ->setParameter('user_id', $user_id)
            ->setParameter('from', $filters['from'])
            ->setParameter('to', $filters['to'])
            ->select('m.id', 'm.datetime', 'm.calories', 'm.text')
            ->getQuery()
            ->getResult();
    }

}